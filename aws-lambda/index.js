'use strict';

var AWS = require('aws-sdk');

exports.handler = (event, context, callback) => {
    let rekognition = new AWS.Rekognition();
    let s3 = new AWS.S3();

    const confidenceThreshold = 70;
    const bucketName = "canipark";
    const uploadsFolder = "test/uploads/";
    const signsConfirmedFolder = "test/signs_confirmed/";
    const othersFolder = "test/others/";
    const resultsFolder = "test/results/";
    let filename;

    if (event.Records) {
        filename = event.Records[0].s3.object.key;
        filename = filename.split('/').pop();
    }
    else 
        filename = event.filename;

    console.log('Received S3 imagename \'' + filename + '\' as parameter.');

    let params = {
        Image: {
            S3Object: {
                Bucket: bucketName,
                Name: uploadsFolder + filename
            }
        }
    };
    
    let finish = (success, message) => {
        callback(null,{
            success: success,
            message: message
        });
    };

    let writeResult = (results) => {
        return (
            s3.putObject(
            {
              Bucket: bucketName,
              Key: resultsFolder + filename.split('.')[0] + '.json',
              Body: results,
              ContentType: "application/json"
            },
            function(err, res){
                if (err) {
                    console.log("Error writing results json to S3.");
                    console.log(err, err.stack); // an error occurred
                    finish(false, "Error copying image.");
                }
            }).promise()
        )
    };

    let moveDetectedImage = (destinationFolder, callback) => {
        var params = {
            Bucket: bucketName,
            CopySource: bucketName + '/' + uploadsFolder + filename,
            Key: destinationFolder + filename
        };
        return (
            s3.copyObject(params, function (err, data) {
                if (err) {
                    console.log("Error copying image to " + destinationFolder + " in S3.");
                    console.log(err, err.stack); // an error occurred
                    finish(false, "Error copying image.");
                } else {
                    console.log("Succesfully copied image to " + destinationFolder + " in S3.");
                    return true;
                }
            })
        )
    };

    let removeImageFromUploads = () => {
        console.log("Removing image '" + filename + "'  from S3 " + uploadsFolder + "folder.");
        var params = {
            Bucket: bucketName,
            Key: uploadsFolder + filename
        };
        return (
            s3.deleteObject(params, function (err, data) {
                if (err) {
                    console.log("Error removing image " + filename + " from " + bucketName + "/" + uploadsFolder + ". Details:");
                    console.log(err, err.stack); // an error occurred
                    finish(false, "Error removing image.");
                } else {
                    console.log("Succesfully removed image " + filename + " from " + bucketName + "/" + uploadsFolder + ".");
                }
            })
        )
    };

    rekognition
        .detectLabels(params, function (err, data) {
            console.log('Detecting labels from image ' + filename);
            if (err) {
                console.log("Error making request to AWS Rekognition Service. Details:");
                console.log(err, err.stack); // an error occurred
                finish(false, "Could not call or finish the Rekognition Service request. Maybe the image is missing?");
            } else { // successful response
                console.log('Rekognition request completed, looking for \'Sign\' label.');

                var destinationFolder = othersFolder;
                data.recognized = false;

                for (var i = 0; i < data.Labels.length; i++) {
                    var label = data.Labels[i];
                    if ((label.Name === 'Sign' || label.Name === 'Street Sign')
                                                     && label.Confidence >= confidenceThreshold) {
                        destinationFolder = signsConfirmedFolder;
                        data.recognized = true;
                        break;
                    }
                }
                
                data.imagePath = destinationFolder + filename;
                data.time = new Date().toString();
                writeResult(JSON.stringify(data))
                    .then(moveDetectedImage(destinationFolder))
                    .then((success) => {
                        if (success)   
                            return removeImageFromUploads()
                    }).then(finish(true, "Finished."))                  
            }
        });
};
