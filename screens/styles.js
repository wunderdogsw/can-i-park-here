import react from 'react'
import { StyleSheet } from 'react-native'

export const homeScreenStyle = StyleSheet.create({
    headerContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: 'skyblue'
    },
    infoContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'skyblue'
    },
    title: {
        textAlign: 'center',
        margin: 10,
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 20
    }
})

export const cameraScreenStyle =  StyleSheet.create({
    container: {
        flex: 1,
    },
    preview: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40,
        alignContent: 'flex-end'
    }
})

export const resultScreenStyle = StyleSheet.create({
    headerContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: 'skyblue'
    },
    infoContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'skyblue'
    },
    title: {
        textAlign: 'center',
        margin: 10,
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 20
    },
    resultText: {
        textAlign: 'center',
        marginBottom: 40,
        color: 'white',
        fontWeight: 'bold',
        fontSize: 60,
    }
})

export const buttonStyle =  StyleSheet.create({
    containerStyle: { 
        padding: 10, 
        height: 45, 
        margin: 10,
        overflow: 'hidden', 
        borderRadius: 4, 
        backgroundColor: 'white'
    },
    default: {
        fontSize: 20, 
        color: 'steelblue' 
    }
})

export const cameraButtonStyle = StyleSheet.create({
    containerStyle: {
        padding: 10,
        height: 45,
        overflow: 'hidden',
        borderRadius: 4,
        margin: 10,
        alignSelf: "stretch"
    },
    default: {
        fontSize: 20,
        color: 'steelblue' 
    }
})