import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Camera from 'react-native-camera'
import Button from 'react-native-button'
import { cameraScreenStyle, cameraButtonStyle } from './styles'

class CameraScreen extends React.Component {
  constructor(props) {
    super(props);
  }
  navigationOptions = {
    title: 'Take a Picture of Street Sign'
  }

  render() {
    return (
      <View style={ cameraScreenStyle.container }>
        <Camera
          ref={(cam) => {
          this.camera = cam;
        }}
          style={ cameraScreenStyle.preview }
          aspect={ Camera.constants.Aspect.fill }
          captureTarget={ Camera.constants.CaptureTarget.temp }
          captureQuality={ Camera.constants.CaptureQuality.photo }>
        </Camera>
        <Button
          containerStyle={ cameraButtonStyle.containerStyle }
          style={ cameraButtonStyle.default }
          onPress={ this.takePicture.bind(this) }>
          Take Picture
      </Button>
      </View>
    )
  }

  takePicture = () => {
    const options = {};
    this.camera.capture({metadata: options})
      .then((data) => { 
        this.props.navigation.navigate('Result', { filepath: data.path }) 
      })
  }
}

export default CameraScreen