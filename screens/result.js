import React from 'react'
import { Text, View } from 'react-native'
import Button from 'react-native-button'
import { NavigationActions } from 'react-navigation'
import Spinner from 'react-native-loading-spinner-overlay'
import { RNS3 } from 'react-native-aws3'
import s3options from '../config/s3-options'
import { resultScreenStyle, buttonStyle } from './styles'

const AWS = require('aws-sdk/dist/aws-sdk-react-native')

AWS.config.update({
    accessKeyId: s3options.accessKey,
    secretAccessKey: s3options.secretKey,
    region: s3options.region
});
const S3 = new AWS.S3()

class Header extends React.Component {
    render() {
        return (
            <Text style={ resultScreenStyle.title }>Result of Traffic Sign Image Analysis</Text>
        )
    }
}

const backAction = NavigationActions.back()

class ResultScreen extends React.Component {
    constructor(props) {
        super(props)
        
        const {params} = this.props.navigation.state
        
        this.state = {
            processing: true,
            spinnerText: "Uploading image...",
            resultText: '',
            file: {
                name: (new Date).getTime() + '.jpg',
                type: "image/jpg",
                uri: params.filepath
            }            
        }

        this.uploadToS3(this.state.file)
    }

    static navigationOptions = {
        title: 'Result'
    }

    handleImageProcessed = (recognized) => {
        this.setState({
            processing: false, 
            resultText: recognized ? ':)' : ':('})
    } 

    pollForResult = () => {
        this.setState({spinnerText: "Processing image..."});
        let timeElapsed = 0;
        that = this;
        // poll for result with 0,5 seconds wait time
        let poller = setInterval(() => {
            var params = {
                Bucket: s3options.bucket, 
                Key: "test/results/" + this.state.file.name.split('.')[0] + '.json'
            };
            S3.getObject(params, function(err, data) {
                if (data) {
                    let dataObj = JSON.parse(data.Body.toString('utf8'));
                    console.log(dataObj);
                    clearInterval(poller);
                    that.handleImageProcessed(dataObj.recognized)
                }
            });
            timeElapsed += 500;
            // Break after 10 seconds in no result
            if (timeElapsed > 10000) {
                clearInterval(poller);
                that.handleImageProcessed(false)
            }
        }, 500)
    }

    uploadToS3 = (file) => {
        RNS3
            .put(file, s3options)
            .then(response => {
                if (response.status !== 201) {
                    throw new Error("Failed to upload image to S3")
                } else {
                    this.pollForResult()
                }
            })
    }

    handlePress() {
        this
            .props
            .navigation
            .dispatch(backAction)
    }

    render() {
        const { navigate } = this.props.navigation
        const { params } = this.props.navigation.state

        return (
            <View style={{ flex: 1 }}>
                <View style={ resultScreenStyle.headerContainer }>
                    <Header/>
                </View>
                <View style={ resultScreenStyle.infoContainer }>
                    <Text style={ resultScreenStyle.resultText }>{ this.state.resultText }</Text>
                    <Button
                        containerStyle={ buttonStyle.containerStyle }
                        style={ buttonStyle.default }
                        onPress={() => this.handlePress()}>
                        Go Back
                    </Button>
                    <Spinner
                        visible={ this.state.processing }
                        textContent={ this.state.spinnerText }
                        textStyle={{ color: '#FFF' }}
                        overlayColor='rgba(0,0,0,0.75)'/>
                </View>
            </View>
        )
    }
}

export default ResultScreen;