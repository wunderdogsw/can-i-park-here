import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Button from 'react-native-button'
import { homeScreenStyle, buttonStyle } from './styles'

class Header extends React.Component {
    render() {
        return (
            <Text style={ homeScreenStyle.title }>Welcome to Can I Park here App!</Text>
        )
    }
}

class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Welcome'
    }

    handlePress() {
        this.props.navigation.navigate('Camera', {});
    }

    render() {
        const { navigate }  = this.props.navigation;

        return (
            <View style={{
                flex: 1
            }}>
                <View style={ homeScreenStyle.headerContainer }>
                    <Header/>
                </View>
                <View style={ homeScreenStyle.infoContainer }>
                    <Button
                        containerStyle={ buttonStyle.containerStyle }
                        style={ buttonStyle.default }
                        styleDisabled={ buttonStyle.styleDisabled }
                        onPress={() => this.handlePress()}>
                        Start Here
                    </Button>
                </View>
            </View>
        )
    }
}

export default HomeScreen