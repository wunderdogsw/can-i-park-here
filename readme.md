# Can I Park application

Tämä on harjoitusprojekti jonka tavoitteena oli toteuttaa applikaatio joka kertoo salliiko liikennemerkki parkkeeraamisen. 

Projekti koostuu mobiiliapplikaatiosta ja AWS:n S3- ja Lambda-instansseista.

Mobiiliapplikaatio on toteutettu React Nativella ja sitä on testattu vain iOS:lla.

Mobiiliapplikaatiolla otetaan kuva joka ladataan S3-buckettiin joka triggeröi Lambdan joka taas käyttää kuvan AWS:n Rekognition-palvelussa. Saatu vastaus kuvan sisällöstä kirjoitetaan JSON-tiedostoon S3:een josta applikaatio lukee vastauksen. Lisäksi kuvat lajitellaan S3:ssa kahteen eri kansioon sen mukaan löysikö Rekognition siitä liikennemerkkiä vai ei.

Projektin aikana tutkittiin TensorFlown hyödyntämistä kuvien spesifimpää tunnistamista varten. Lisätietoja [täällä](https://github.com/wunderdogsw/tensorflow-image-classification-tutorial).

Tällä hetkellä kuvasta saadaan selville vain onko siinä liikennemerkki vai ei.

AWS:n lambda-koodi löytyy *aws-lambda* -kansiosta. Lambdafunktio löytyy WD:n aws-sandboxista nimellä *kona-canipark-hellp*. S3 bucketin nimi on '*canipark*'.

### TODO
* Tee/löydä nykyistä parempi palvelu/kirjasto, jolla voit oikeasti tunnistaa kuvassa olevasta liikennemerkistä mitä se sisältää ja kerro se applikaatiossa käyttäjälle
* Puhelimien kamerat ovat laajakulmaisia, jonka takia kuvaan tulee lähes aina paljon "muuta sälää". Tämä vaikeuttaa ja heikentää liikennemerkin tunnistamista ja tekstien lukemista. Olisi siis hyvä jos kuvan voisi esikäsitellä niin että siitä poistettaisiin "turhat" osat. Yksi ratkaisu voisi olla croppaaminen niin että kuvassa oleva merkki näkyy selkeämmin. Nykyiset pixelimäärät antavat mahdollisuuden tähän ilman että kuva menee liian epäselväksi.
* kuvien tunnistusten tulokset tallennetaan omiksi JSON-tiedostoiksi joita applikaatio lukee S3:sta. Tämän voisi muuttaa niin että tiedot menevät johonkin tietokantaan jolloin datan esim. lukeminen massana ja kyselyiden tekeminen onnistuisi.

### Requirements
* React >= 16.0.0-alpha.12
* React Native >= 0.48.3
* Xcode

### Running
`react-native run-ios` & avaa projekti Xcodessa & 'run on device'