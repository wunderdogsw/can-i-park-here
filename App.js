import React from 'react';
import { AppRegistry, Button, Text, View } from 'react-native';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './screens/home';
import CameraScreen from './screens/camera'
import ResultScreen from './screens/result'

export default CanIPark = StackNavigator({
  Home: {
    screen: HomeScreen
  },
  Camera: {
    screen: CameraScreen
  },
  Result: {
    screen: ResultScreen
  },
});

// if you are using create-react-native-app you don't need this line
AppRegistry.registerComponent('CanIPark', () => CanIPark);